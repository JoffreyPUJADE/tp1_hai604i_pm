#include "../../include/client/InclusionsClient.h"
#include <stdlib.h>
#include<string.h>

/*
	Programme client
*/

int main(int argc, char *argv[])
{
	init();
	
	/*
		je passe en paramètre l'adresse de la socket du serveur (IP et
		numéro de port) et un numéro de port à donner à la socket créée plus loin.
	*/
	
	/*
		Je teste le passage de parametres. Le nombre et la nature des
		paramètres sont à adapter en fonction des besoins. Sans ces
		paramètres, l'exécution doit être arrétée, autrement, elle
		aboutira à des erreurs.
	*/
	if (argc != 4)
	{
		printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
		exit(1);
	}
	
	/*
		Etape 1 : créer une socket
	*/   
	int ds = socket(PF_INET, SOCK_DGRAM, 0);
	
	/*
		/!\ : Il est indispensable de tester les valeurs de retour de
		toutes les fonctions et agir en fonction des valeurs
		possibles. Voici un exemple
	*/
	//if (ds == -1)
	if (ds == INVALID_SOCKET)
	{
		perror("Client : pb creation socket :");
		exit(1); // je choisis ici d'arrêter le programme car le reste
				 // dépendent de la réussite de la création de la socket.
	}
	
	/*
		J'ajoute des traces pour comprendre l'exécution et savoir
		localiser des éventuelles erreurs
	*/
	printf("Client : creation de la socket réussie \n");
	
	// Je peux tester l'exécution de cette étape avant de passer à la
	// suite. Faire de même pour la suite : n'attendez pas de tout faire
	// avant de tester.
	
	/*
		Etape 2 : Nommer la socket du client
	*/
	
	SOCKADDR_IN ad = { 0 };
	
	ad.sin_addr.s_addr = INADDR_ANY;
	ad.sin_port = htons((short)0);
	ad.sin_family = AF_INET;
	
	int resBind = bind(ds, ((SOCKADDR*)&ad), sizeof(ad));
	
	if(resBind == SOCKET_ERROR)
	{
		int errBind = ERRNO_VALUE;
		
		traitementErreurBind(errBind);
	}
	
	int addrlen = sizeof(ad);
	
	int sn = getsockname(ds, ((SOCKADDR*)&ad), &addrlen);
	
	if(sn == SOCKET_ERROR)
	{
		int errGetSockName = ERRNO_VALUE;
		
		traitementErreurGetsockname(errGetSockName);
	}
	
	/*
		Etape 3 : Désigner la socket du serveur
	*/
	
	// Création des informations du serveur.
	SOCKADDR_IN sin = { 0 };
	
	sin.sin_addr.s_addr = inet_addr(argv[1]); // Adresse du serveur, premier argument passé en paramètre.
	sin.sin_port = htons(atoi(argv[2])); // Port du serveur, deuxième argument passé en paramètre.
	sin.sin_family = AF_INET;
	
	int resConnect = connect(ds, ((SOCKADDR *)&sin), sizeof(SOCKADDR));
	
	if(resConnect == SOCKET_ERROR)
	{
		int errConnect = ERRNO_VALUE;
		
		traitementErreurConnect(errConnect);
	}
	
	/*
		Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)
	*/
	
	int tailleMessageAEnvoyer = 101;
	char messageAEnvoyer[tailleMessageAEnvoyer];
	
	printf("Veuillez saisir un message a envoyer au serveur, au maximum %d caractere.s :\n", tailleMessageAEnvoyer);
	fgets(messageAEnvoyer, tailleMessageAEnvoyer, stdin);
	
	int resSendTo = sendto(ds, messageAEnvoyer, sizeof(char) * tailleMessageAEnvoyer, 0, ((SOCKADDR*)&sin), sizeof(sin));
	
	if(resSendTo == SOCKET_ERROR)
	{
		int errSendTo = ERRNO_VALUE;
		
		traitementErreurSendto(errSendTo);
	}
	
	size_t tailleMessageARecevoir = 3;
	char messageOk[tailleMessageARecevoir];
	int taille = sizeof(ad);
	
	ssize_t res = recvfrom(ds, messageOk, sizeof(char) * tailleMessageARecevoir, 0, ((SOCKADDR *)&ad), &taille);
	
	printf("%s\n", messageOk);
	
	/*
		Etape 5 : recevoir un message du serveur (voir sujet pour plus de détails)
	*/
	
	/*
		Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)
	*/
	
	close(ds);
	
	
	printf("Client : je termine\n");
	
	end();
	
	return 0;
}
