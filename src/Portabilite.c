#include "../include/Portabilite.h"

// Code source copié du site https://broux.developpez.com/articles/c/sockets/#LII-C

void init(void)
{
	#ifdef WIN32
		WSADATA wsa;
		int err = WSAStartup(MAKEWORD(2, 2), &wsa);
		if(err < 0)
		{
			puts("WSAStartup failed !");
			exit(EXIT_FAILURE);
		}
	#endif
}

void end(void)
{
	#ifdef WIN32
		WSACleanup();
	#endif
}

/*
	Les fonctions qui suivent permettent le traitement des codes d'erreurs de certaines fonctions, de
	manière plus ou moins portable entre Windows et Linux, sans pour autant alourdir le main.
*/

void traitementErreurBind(int codeErreur)
{
	switch(codeErreur)
	{
		case ADDR_IN_USE:
			printf("ERREUR : Adresse deja utilisee.");
			exit(1);
		break;
		
		case IN_VAL:
			printf("ERREUR : La socket est deja utilisee.");
			exit(1);
		break;
		
		case NOT_SOCK:
			printf("ERREUR : Ce n'est pas un descripteur de socket.");
			exit(1);
		break;
		
		case ACCES:
			printf("ERREUR : Adresse protegee.");
			exit(1);
		break;
		
		case ADDR_NOT_AVAIL:
			printf("ERREUR : Adresse non disponible.");
			exit(1);
		break;
		
		case FAULT:
			printf("ERREUR : Pointeur d'adresse invalide.");
			exit(1);
		break;
		
		case 0:
		break;
		
		default:
			printf("ERREUR : Une erreur de type inconnue s'est produite.");
		break;
	}
}

void traitementErreurGetsockname(int codeErreur)
{
	switch(codeErreur)
	{
		case FAULT:
			printf("ERREUR : Pointeur d'adresse invalide.");
			exit(1);
		break;
		
		case IN_VAL:
			printf("ERREUR : La socket est deja utilisee.");
			exit(1);
		break;
		
		case NOT_SOCK:
			printf("ERREUR : Ce n'est pas un descripteur de socket.");
			exit(1);
		break;
		
		case 0:
		break;
		
		default:
			printf("ERREUR : Une erreur de type inconnue s'est produite.");
		break;
	}
}

void traitementErreurConnect(int codeErreur)
{
	switch(codeErreur)
	{
		case ACCES:
			printf("ERREUR : Adresse protegee.");
			exit(1);
		break;
		
		case ADDR_IN_USE:
			printf("ERREUR : Adresse deja utilisee.");
			exit(1);
		break;
		
		case ADDR_NOT_AVAIL:
			printf("ERREUR : Adresse non disponible.");
			exit(1);
		break;
		
		case F_NO_SUPPORT:
			printf("ERREUR : Famille d'adresse incompatible avec l'adresse fournie.");
			exit(1);
		break;
		
		case WOULD_BLOCK:
			printf("ERREUR : La connexion ne peut pas etre complete immediatement alors que la socket est marquee comme non-bloquante.");
			exit(1);
		break;
		
		case CONN_REFUSED:
			printf("ERREUR : La connexion a ete rejetee.");
			exit(1);
		break;
		
		case FAULT:
			printf("ERREUR : Pointeur d'adresse invalide.");
			exit(1);
		break;
		
		case IN_PROGRESS:
			printf("ERREUR : La connexion ne peut pas etre complete immediatement alors que la socket est marquee comme non-bloquante. La connexion etait en attente.");
			exit(1);
		break;
		
		case INTR:
			printf("ERREUR : L'appel systeme a ete interrompu.");
			exit(1);
		break;
		
		case NET_UNREACH:
			printf("ERREUR : Le reseau est inaccessible.");
			exit(1);
		break;
		
		case NOT_SOCK:
			printf("ERREUR : Ce n'est pas un descripteur de socket.");
			exit(1);
		break;
		
		case TIMED_OUT:
			printf("ERREUR : La duree de connexion est ecoulee.");
			exit(1);
		break;
		
		/*case IN_VAL:
			printf("ERREUR : La socket est deja utilisee.");
			exit(1);
		break;*/
		
		case 0:
		break;
		
		default:
			printf("ERREUR : Une erreur de type inconnue s'est produite.");
		break;
	}
}

void traitementErreurSendto(int codeErreur)
{
	switch(codeErreur)
	{
		case ACCES:
			printf("ERREUR : Adresse protegee.");
			exit(1);
		break;
		
		case WOULD_BLOCK:
			printf("ERREUR : La connexion ne peut pas etre complete immediatement alors que la socket est marquee comme non-bloquante.");
			exit(1);
		break;
		
		case ADDR_IN_USE:
			printf("ERREUR : Adresse deja utilisee.");
			exit(1);
		break;
		
		case ADDR_NOT_AVAIL:
			printf("ERREUR : Adresse non disponible.");
			exit(1);
		break;
		
		case F_NO_SUPPORT:
			printf("ERREUR : Famille d'adresse incompatible avec l'adresse fournie.");
			exit(1);
		break;
		
		case CONN_REFUSED:
			printf("ERREUR : La connexion a ete rejetee.");
			exit(1);
		break;
		
		case FAULT:
			printf("ERREUR : Pointeur d'adresse invalide.");
			exit(1);
		break;
		
		case IN_PROGRESS:
			printf("ERREUR : La connexion ne peut pas etre complete immediatement alors que la socket est marquee comme non-bloquante. La connexion etait en attente.");
			exit(1);
		break;
		
		case INTR:
			printf("ERREUR : L'appel systeme a ete interrompu.");
			exit(1);
		break;
		
		case NET_UNREACH:
			printf("ERREUR : Le reseau est inaccessible.");
			exit(1);
		break;
		
		case NOT_SOCK:
			printf("ERREUR : Ce n'est pas un descripteur de socket.");
			exit(1);
		break;
		
		case TIMED_OUT:
			printf("ERREUR : La duree de connexion est ecoulee.");
			exit(1);
		break;
		
		/*case IN_VAL:
			printf("ERREUR : La socket est deja utilisee.");
			exit(1);
		break;*/
		
		case 0:
		break;
		
		default:
			printf("ERREUR : Une erreur de type inconnue s'est produite.");
		break;
	}
}