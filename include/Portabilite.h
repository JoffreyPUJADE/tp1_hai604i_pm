#ifndef PORTABILITE_H
#define PORTABILITE_H

// Code source copié du site https://broux.developpez.com/articles/c/sockets/#LII-C

#ifdef WIN32 /* si vous êtes sous Windows */
	#include <winsock2.h>
	
	#define close(s) closesocket(s)
#elif defined (linux) /* si vous êtes sous Linux */
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <unistd.h> /* close */
	#include <netdb.h> /* gethostbyname */
	
	#define INVALID_SOCKET -1
	#define SOCKET_ERROR -1
	
	typedef int SOCKET;
	typedef struct sockaddr_in SOCKADDR_IN;
	typedef struct sockaddr SOCKADDR;
	typedef struct in_addr IN_ADDR;
#else /* sinon vous êtes sur une plateforme non supportée */
	#error not defined for this platform
#endif

#include <stdio.h>

void init(void);
void end(void);

// Adaptation des codes d'erreur selon les documentations de l'API Windows et les man de Linux (tous deux vus en ligne).

#if defined(linux)
	#include <errno.h>
#endif

enum ErrSocks // Énumération des codes d'erreur plus ou moins communes à Windows et Linux au niveau des sockets.
{
	#if defined(WIN32)
		ADDR_IN_USE = WSAEADDRINUSE,
		IN_VAL = WSAEINVAL,
		NOT_SOCK = WSAENOTSOCK,
		ACCES = WSAEACCES,
		ADDR_NOT_AVAIL = WSAEADDRNOTAVAIL,
		FAULT = WSAEFAULT,
		F_NO_SUPPORT = WSAEAFNOSUPPORT,
		ALREADY = WSAEALREADY,
		TIMED_OUT = WSAETIMEDOUT,
		NET_UNREACH = WSAENETUNREACH,
		CONN_REFUSED = WSAECONNREFUSED,
		INTR = WSAEINTR,
		WOULD_BLOCK = WSAEWOULDBLOCK,
		IN_PROGRESS = WSAEINPROGRESS,
		CONN_RESET = WSAENETRESET
	#elif defined(linux)
		ADDR_IN_USE = ADDRINUSE,
		IN_VAL = EINVAL,
		NOT_SOCK = ENOTSOCK,
		ACCES = EACCES,
		ADDR_NOT_AVAIL = EADDRNOTAVAIL,
		FAULT = EFAULT,
		F_NO_SUPPORT = EAFNOSUPPORT,
		ALREADY = EALREADY,
		TIMED_OUT = ETIMEDOUT,
		NET_UNREACH = WSAENETUNREACH,
		CONN_REFUSED = ECONNREFUSED,
		INTR = EINTR,
		WOULD_BLOCK = EAGAIN,
		IN_PROGRESS = EINPROGRESS,
		CONN_RESET = ECONNRESET
	#endif
};

#ifndef ERRNO_VALUE
	#if defined(WIN32)
		#define ERRNO_VALUE WSAGetLastError()
	#elif defined(linux)
		#define ERRNO_VALUE errno
	#endif
#endif // ERRNO_VALUE

void traitementErreurBind(int codeErreur);
void traitementErreurGetsockname(int codeErreur);
void traitementErreurConnect(int codeErreur);
void traitementErreurSendto(int codeErreur);

#endif // PORTABILITE_H