# TP1_HAI604I_PM

Programmation du premier TP de l'UE HAI604I - Programmation Multitâches.

# Sommaire

* [Architecture du TP](#architecture-du-tp)
* [Compilation](#compilation)
* [Exécution](#exécution)
* [Attention](#attention)
* [Sources](#sources)

# Architecture du TP

Le TP est réparti selon une arborescence spécifique, que voici :

	include/ # Répertoire contenant tous les fichiers d'en-tête confondus du TP. Tout ce qui se trouve à sa racine est destiné à tout le projet.
		client/ # Répertoire contenant tous les fichiers d'en-tête spécifique au client.
			InclusionsClient.h # Fichier d'en-tête regroupant, dans un seul fichier, tous les fichiers d'en-tête créés à inclure pour le client.
		serveur/ # Répertoire contenant tous les fichiers d'en-tête spécifique au serveur.
			InclusionsServeur.h # Fichier d'en-tête regroupant, dans un seul fichier, tous les fichiers d'en-tête créés à inclure pour le serveur.
		Portabilite.h # Fichier d'en-tête assurant la portabilité des sockets entre Linux et Windows.
	ressources/ # Répertoire contenant les fichiers aidant ou étant nécessaires à la réalisation du TP.
		base/ # Répertoire contenant le sujet ainsi que les fichiers de départ du TP.
			fournisTP1.tgz # Archive contenant les fichiers de base pour la réalisation du TP.
			sujetTP1.pdf # Fichier contenant le sujet du TP.
	src/ # Répertoire contenant tous les fichiers source confondus du TP. Tout ce qui se trouve à sa racine est destiné à tout le projet.
		client/ # Répertoire contenant tous les fichiers source spécifique au client.
			client.c # Fichier source contenant le `main` du client.
		serveur/ # Répertoire contenant tous les fichiers source spécifique au serveur.
			serveur.c # Fichier source contenant le `main` du serveur.
		Portabilite.c # Fichier source assurant la portabilité des sockets entre Linux et Windows.
	.gitignore
	make.bat # Fichier contenant une commande permettant de lancer le Makefile sous Windows plus facilement en ligne de commandes.
	Makefile # Makefile adapté de celui fourni ainsi que d'anciens que j'avais réalisé auparavant. Il est compatible avec Linux et Windows (si MinGW est utilisé).
	README.md

# Compilation

Le code source étant cross-platform entre Linux et Windows, il n'y à qu'à utiliser la commande `make` en ligne de commandes
afin de lancer la compilation.

Cependant, la compilation génèrera les deux exécutables dans la racine, par souci de simplicité. Néanmoins, il est possible
de modifier le Makefile afin de générer les deux exécutables dans un quelconque fichier `bin`.

# Exécution

Pour l'exécution, il n'y a aucune différence entre Linux et Windows, excepté l'extension `.exe` utilisée sous Windows
et pas sous Linux.

# Attention

Cette version du code source n'est pas encore complète, elle est tout juste fonctionnelle.

Les tests n'ont pas encore été faits sous Linux.

# Sources

Pour créer les fichiers `Portabilite.h` et `Portabilite.c` m'ayant servi pour la portabilité entre Linux et Windows, je me
suis servi de la partie [II-C. Les sockets portables Windows/Linux](https://broux.developpez.com/articles/c/sockets/#LII-B) du cours [Les sockets en C](https://broux.developpez.com/articles/c/sockets/) disponible sur le site `developpez.com`.